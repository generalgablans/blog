class Comment < ActiveRecord::Base
  belongs_to :post
  validates_presecene_of :post_id
  validates_presence_pf :body
end
